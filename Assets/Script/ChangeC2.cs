﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeC2 : MonoBehaviour 
{
    public SpriteRenderer renderer;
    public Sprite sprite1;
    public Sprite sprite2;
    public Sprite sprite3;
	public Sprite sprite4;
    public string nextSceneName;

    private void OnMouseDown()
    {
		if (renderer.sprite == sprite1) {
			renderer.sprite = sprite2;
		} else if (renderer.sprite == sprite2) {
			renderer.sprite = sprite3;
		} else if (renderer.sprite == sprite3) {
			renderer.sprite = sprite4;
		}
		else if (renderer.sprite == sprite4)
		{ 
			Application.LoadLevel(nextSceneName);
    	}    
	}
}
