﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeCutScene : MonoBehaviour
{
   public SpriteRenderer renderer;
   public Sprite sprite1;
   public Sprite sprite2;
   public Sprite sprite3;
   public string nextSceneName;

    private void OnMouseDown()
    {
        if (renderer.sprite == sprite1)
        {
            renderer.sprite = sprite2;
        }
        else if(renderer.sprite == sprite2)
        {
            Application.LoadLevel(nextSceneName);
        }
    }
}
