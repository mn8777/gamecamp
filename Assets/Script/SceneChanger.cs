﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SceneChanger : MonoBehaviour
{
	public string nextSceneName;

	void OnMouseDown () 
	{
		Application.LoadLevel (nextSceneName);
	}
}