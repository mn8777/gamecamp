﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AI : MonoBehaviour
{
    public float movePower = 1f;

    //Animator animator;
    Vector3 movement;
    int movementFlag = 0;

    void Start ()
    {
        StartCoroutine("ChangeMovement");
    }
 
    void Update ()
    {
        Move();
    }
    void Move()
    {
        Vector3 moveVelocity = Vector3.zero;

        if (movementFlag == 1)
        {
            moveVelocity = Vector3.left;
            //transform.localScale = new Vector3(1, 1, 1);
        }
        else if (movementFlag == 2)
        {
            moveVelocity = Vector3.right;
            //transform.localScale = new Vector3(-1, 1, 1);
        }
        transform.position += moveVelocity * movePower * Time.deltaTime;
    }
    IEnumerator ChangeMovement()
    {
        //Random Change Movement
        movementFlag = Random.Range(0, 3);

        /*Mapping Amination
         * if (movementFlag == 0)
         * animator.SetBool ("isMoveing",flase);
         * else 
         * animator.SetBool ("isMoving",true);*/

        //Debug.Log("Front Logic : " + Time.time);
        //Wait 3 Seconds
        yield return new WaitForSeconds(1f);
        //Debug.Log("Behind Logic : " + Time.time);

        //Restart Logic
        StartCoroutine("ChangeMovement");
    }
    /*//Trace Start
    void onTriggerEnter2D (collider2D other)
    {
        if (creaturetype == 0)
            return;
        if (other.gameObject.tag == "Plater")
        {
            traceTarget = other.gameObject;
            StopCoroutine("ChangeMovement");
        }
    }

    //Trace Maintain
    void OnTriggerStay2D (collider2D other)
    {
        if (creatureType == 0)
            return;
        if (other.gameObject.tag == "Player")
        {
            isTracing = true;
        }
    }

    //Trace Over
    void OnTriggerExit2D(collider2D other)
    {
        if (creatureType == 0)
            return;
        if (other.gameObject.tag == 0)
        {
            isTracing = false;
            StartCoroutine("ChangeMovement");
        }
    }
    void Move ()
    {
        Vector3 moveVelocity = Vector3.zero;
        string dist = "";

        //Trace or Random
        if (isTracing)
        {
            Vector3 playerpos = traceTarget.transform.position;

            if (playerpos.x < transform.position.x)
                dist = "Left";
            else if (playerpos.x > transform.position.x)
                dist = "Right";
        }
        else
        {
            if (movementFlag == 1)
                dist = "Left";
            else if (movementFlag == 1)
                dist = "Right";

        }
    }*/

}
