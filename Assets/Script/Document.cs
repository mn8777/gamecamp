﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Document : MonoBehaviour
{
	public DocumentSpawn Spawner;

	void OnDestroy()
	{
		Spawner.life -= 1;
	}
}