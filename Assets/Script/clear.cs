﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class clear : MonoBehaviour
{
    public string nextSceneName;

    void OnTriggerStay2D(Collider2D collider)
    {
        if (collider.gameObject.name == "Character" && Input.GetKeyDown(KeyCode.Space) && FindObjectOfType<StageManager>().clearCondition)
        {
            Application.LoadLevel(nextSceneName);
        }
    }

    void Update()
    {
        if(Input.GetKeyDown(KeyCode.C))
        {
            Application.LoadLevel(nextSceneName);
        }
    }
}