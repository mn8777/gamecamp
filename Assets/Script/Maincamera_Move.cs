﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Maincamera_Move : MonoBehaviour
{
    public GameObject A;
    Transform FT;
	// Use this for initialization
	void Start ()
    {
        FT = A.transform;
	}

    // Update is called once per frame
    void LateUpdate()
    {
        transform.position = new Vector3(FT.position.x, FT.position.y, FT.position.z-10);
    }
}
