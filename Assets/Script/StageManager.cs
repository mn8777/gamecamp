﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StageManager : MonoBehaviour 
{
	public float timeLimit;
	bool isAppeared = false;
	public Supervisor SupervisorPrefab;
	public enum StageIndex{Stage2, Stage3, Stage4, Else};
	public StageIndex Stage;
	public TextMesh textMesh;
	public string NextStage;
	public bool clearCondition;
	public bool[] PaperFound;

	void Update()
	{
		timeLimit -= Time.deltaTime;

		if (Stage == StageIndex.Stage2) 
		{
			if (timeLimit < 0 && !isAppeared)
			{
				isAppeared = true;
				Instantiate (SupervisorPrefab);
			}
		}

		if (Stage == StageIndex.Stage3 && timeLimit < 0 && !isAppeared) 
		{
			isAppeared = true;
			Instantiate (SupervisorPrefab);
		}

		if(Stage == StageIndex.Stage4 && timeLimit < 0)
		{
			Application.LoadLevel("GameOver");
		}
	}

	public IEnumerator GoNext(float delay)
	{
		yield return new WaitForSeconds (delay);
		Application.LoadLevel (NextStage);
	}

	public void CheckPapers()
	{
		bool result = true;
		foreach(bool Paper in PaperFound)
		{
			if(!Paper)
			{
				result = false;
			}
		}
		clearCondition = result;
	}
}