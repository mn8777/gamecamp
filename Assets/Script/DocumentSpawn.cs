﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DocumentSpawn : MonoBehaviour
{
	public int life;
	public int documentCount;
	public Document document;
	Document Instance;
	public SpriteRenderer[] renderers;

	void Start()
	{
		StartCoroutine ("Spawn");
	}

	void Update()
	{
		for (int i = 1; i <= renderers.Length; i++) 
		{
			if (life < i) 
			{
				renderers [i-1].enabled = false;
			}
		}
	}

	IEnumerator Spawn()
	{
		Instance = Instantiate (document, new Vector3 (Random.Range (-8.0f, 8.0f), document.transform.position.y, document.transform.position.z), Quaternion.identity) as Document;
		Instance.Spawner = this;
		if (documentCount > 0)
		{
			yield return new WaitForSeconds(0.5f);
			documentCount -= 1;
			StartCoroutine ("Spawn");
		}
	}
}