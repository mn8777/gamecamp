﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Paper : MonoBehaviour
{
	public TextMesh Text;
	public float timer;

	void Update()
	{
		timer -= Time.deltaTime;
		if (timer < 0) 
		{
			Destroy (gameObject);
		}
	}
}