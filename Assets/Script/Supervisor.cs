﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Supervisor : MonoBehaviour
{
	float speed = 3f;
	public int MovePhase;

	void Update()
	{
		if (MovePhase == 0) 
		{
			Vector3 position = gameObject.transform.position;
			position.x -= Time.deltaTime * speed;
			gameObject.transform.position = position;
			if (position.x < -9) 
			{
				MovePhase += 1;
			}
		}
		else if (MovePhase == 1)
		{
			Vector3 position = gameObject.transform.position;
			position.x += Time.deltaTime * speed;
			gameObject.transform.position = position;
			if (position.x > 8) 
			{
				MovePhase += 1;
			}
		}
		else if (MovePhase == 2) 
		{
			Vector3 position = gameObject.transform.position;
			position.y -= Time.deltaTime * speed;
			gameObject.transform.position = position;
			if (position.y < -1.3f) 
			{
				MovePhase += 1;
			}
		}
		else
		{
			Vector3 position = gameObject.transform.position;
			position.x -= Time.deltaTime * speed;
			gameObject.transform.position = position;
		}
	}

	void OnTriggerEnter2D(Collider2D collider)
	{
		if(collider.gameObject.name == "Character")
		{
			if(FindObjectOfType<StageManager>().Stage == StageManager.StageIndex.Stage2)
			{
				Application.LoadLevel ("CutScene2");
			}
			else
			{
				Application.LoadLevel("GameOver");
			}
		}
	}
}