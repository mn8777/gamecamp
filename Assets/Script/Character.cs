﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Character : MonoBehaviour
{
	public Rigidbody2D rigidbody;
	public Collider2D[] Ladders;
	public Collider2D[] Lands;
	public bool verticalMove;
	bool isMenuOpen = false;
	bool isLadderUsing = false;
	public bool isJumping = false;
	public float timer;
	public Paper PaperPrefab;
	Paper PaperInstance;
	public StageManager Manager;

	void Update ()
    {
		if (Input.GetKeyDown (KeyCode.D))
        {
            Vector3 position = gameObject.transform.position;
            position.x += 0.5f;
            gameObject.transform.position = position;
        }
		if (Input.GetKeyDown (KeyCode.A)) 
		{
			Vector3 position = gameObject.transform.position;
			position.x += -0.5f;
			gameObject.transform.position = position;
		}
		if (Input.GetKeyDown (KeyCode.W) && verticalMove)
		{
			if (!isLadderUsing)
			{
				if (!isJumping) 
				{
					isJumping = true;
					rigidbody.gravityScale = -1;
					StartCoroutine (Drop ());
				}
			}
			else
			{
				Vector3 position = gameObject.transform.position;
				position.y += 1;
				gameObject.transform.position = position;
				rigidbody.gravityScale = 0;
			}
		}
		if (Input.GetKeyDown(KeyCode.S) && verticalMove)
		{
	        Vector3 position = gameObject.transform.position;
			position.y += -1;
			gameObject.transform.position = position;
		}
		if (Input.GetKeyDown (KeyCode.Space)) 
		{
			RaycastHit2D[] hit = Physics2D.RaycastAll (transform.position, new Vector2 (0, 0));
			bool BoxCollision = false;
			for (int i = 0; i < hit.Length; i++) 
			{
				if (hit [i].collider.tag == "Box")
				{
					BoxCollision = true;
				}
			}

			if (BoxCollision)
			{
				if (CheckSpace (hit, "Box1-3") != null) 
				{
					PaperInstance = Instantiate (PaperPrefab, PaperPrefab.transform.position, Quaternion.identity) as Paper;
					PaperInstance.Text.text = "3";
					FindObjectOfType<StageManager>().PaperFound[0] = true;
					FindObjectOfType<StageManager>().CheckPapers();
				}
				else if (CheckSpace (hit, "Box2-2") != null) 
				{
					PaperInstance = Instantiate (PaperPrefab, PaperPrefab.transform.position, Quaternion.identity) as Paper;
					PaperInstance.Text.text = "2";
					FindObjectOfType<StageManager>().PaperFound[1] = true;
					FindObjectOfType<StageManager>().CheckPapers();
				}
				else if (CheckSpace (hit, "Box3-4") != null) 
				{
					PaperInstance = Instantiate (PaperPrefab, PaperPrefab.transform.position, Quaternion.identity) as Paper;
					PaperInstance.Text.text = "1";
					FindObjectOfType<StageManager>().PaperFound[2] = true;
					FindObjectOfType<StageManager>().CheckPapers();
				}
				else 
				{
					PaperInstance = Instantiate (PaperPrefab, PaperPrefab.transform.position, Quaternion.identity) as Paper;
				}
			}
		}
    }

	IEnumerator ChangeScene(float delay, string sceneName)
	{
		yield return new WaitForSeconds (delay);
		Application.LoadLevel (sceneName);
	}

	GameObject CheckSpace(RaycastHit2D[] hit, string name)
	{
		for (int i = 0; i < hit.Length; i++) 
		{
			if (hit [i].collider.name == name) 
			{
				return hit [i].collider.gameObject;
			}
		}
		return null;
	}

	void OnTriggerEnter2D(Collider2D collider)
	{
		if (collider.gameObject.name == "Ladder") 
		{
			isLadderUsing = true;
			rigidbody.gravityScale = 0;
			for (int i = 0; i < Ladders.Length; i++) 
			{
				if (collider == Ladders[i]) 
				{
					Lands [i].enabled = false;
				}
			}
		}
		if (collider.gameObject.name == "Document(Clone)") 
		{
			Destroy (collider.gameObject);
		}
	}

	void OnTriggerExit2D(Collider2D collider)
	{
		if (collider.gameObject.name == "Ladder") 
		{
			isLadderUsing = false;
			for (int i = 0; i < Lands.Length; i++) 
			{
				Lands [i].enabled = true;
			}
			rigidbody.gravityScale = 1;
		}
	}

	IEnumerator Drop()
	{
		yield return new WaitForSeconds (0.3f);
		rigidbody.gravityScale = 1.0f;
		yield return new WaitForSeconds (0.8f);
		isJumping = false;
	}
}