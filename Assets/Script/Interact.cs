﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Interact : MonoBehaviour 
{
	public bool isReal;
	public bool doesChangeText;
	public bool ContactOrSpace;
	public string AlterText;
	public enum InteractionTypeIndex{Destroy, ChangeSprite, None, CutTime};
	public InteractionTypeIndex InteractionType;
	SpriteRenderer renderer; 
	public Sprite AlterSprite;

	void Start()
	{
		renderer = gameObject.GetComponent<SpriteRenderer>();
	}
	void OnTriggerStay2D (Collider2D collider)
	{
		if(!ContactOrSpace && collider.gameObject.name == "Character" && Input.GetKeyDown(KeyCode.Space))
		{
			DoInteract();	
		}
	}

	void OnTriggerEnter2D (Collider2D collider)
	{
		if(ContactOrSpace)
		{
			DoInteract();
		}
	}

	void DoInteract()
	{
		if(isReal)
		{
			FindObjectOfType<StageManager>().clearCondition = true;
		}

		if(InteractionType == InteractionTypeIndex.Destroy)
		{
			Destroy(gameObject);
		}
		else if(InteractionType == InteractionTypeIndex.ChangeSprite)
		{
			renderer.sprite = AlterSprite;
		}
		else if(InteractionType == InteractionTypeIndex.CutTime)
		{
			FindObjectOfType<StageManager>().timeLimit -= 10;
		}
		if(doesChangeText)
		{
			FindObjectOfType<StageManager>().textMesh.text = AlterText;
		}
	}
}
