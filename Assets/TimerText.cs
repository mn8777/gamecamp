﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimerText : MonoBehaviour 
{
	public TextMesh textMesh;
	void Update () 
	{
		textMesh.text = FindObjectOfType<StageManager>().timeLimit.ToString();
	}
}
